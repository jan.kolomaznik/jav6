package jm.test.spock.exercise

import spock.lang.Unroll

class ImplCarServiceTest extends spock.lang.Specification {

    ImplCarService carService;
    CarRepository carRepository

    def setup() {
        carService = new ImplCarService()
        carService.carRepository = carRepository = Mock(CarRepository)
    }

    def "createCar"() {
        given:
        def car = new Car(name: "Skodovka")
        carRepository.save(_ as Car) >> Optional.of(car)

        when:
        def result = carService.createCar("Skodovka")

        then:
        result == car
        1 * carRepository.save(car) >> { Car arg ->
            arg.id = 5
            return Optional.of(arg)
        }
        result.id == 5
    }

    def "RentCar"() {
    }

    def "create car id when is all correct."() {
        setup:
        def carService = new ImplCarService()
        when:
        def result = carService.createId("SPZ 0123")
        then:
        result == 1
    }

    @Unroll
    def "private uniform(#name) = #result"() {
        expect: """Komentář ..."""
        carService.uniform(name) == result

        where: """
            Cice 
            rasdcích
            
        """
        name        | result
        "ABC"       | "ABC"
        " SPZ " | "SPZ" // komentář
        "spz"  | "SPZ"
    }

}
