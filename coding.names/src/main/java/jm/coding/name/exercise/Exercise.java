package jm.coding.name.exercise;

public class Exercise {


    public static void main(String[] args) {
        // PUT api/nasedni/{carId = 1}/{zakaznikId = 2}
        Id<Auto> cadId = Id.of(1);
        Id<Zakaznik> zakaznikId = Id.of(2);

        String msg = nasedni(zakaznikId, cadId);
        System.out.println(msg);

    }

    public static String nasedni(Id<Zakaznik> zakaznikId, Id<Auto> carId) {
        Zakaznik z = Zakaznik.getZakaznik(zakaznikId);
        Auto a = Auto.getAuto(carId);
        return String.format("Zakaznik %s nasedl do auta %s.", z.getName(), a.getName());
    }
}
