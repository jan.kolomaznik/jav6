package jm.coding.name.exercise;

public class Id<T> {

    public static <E> Id<E> of(long id) {
        return new Id<>(id);
    }

    private long id;

    private Id(long id) {
        this.id = id;
    }

    public long getValue() {
        return id;
    }
}
