package jm.design.patterns;

public class Acounts {

    private long balance;
    private long loan;

    public void putMoney(long amount) {
        if (loan < 0) {
            this.loan -= amount;
        } else {
            this.balance += amount;
        }
    }

    public void drawMoney(long amount) {
        if (balance > amount) {
            this.balance -= amount;
        } else if (loan > amount) {
            this.loan -= amount;
        }
    }

    public void takeLoan(long amount) {
        if (loan == 0) {
            this.loan = amount;
        }
    }
}
