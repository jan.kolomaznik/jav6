package cz.mendelu.ppr;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Mutex {

    public static void main(String[] args) {


    }

    static void mutex_1a() {

        long shared = 5l;
        Object mShared = new Object();

        synchronized (mShared) {
            System.out.println(++shared);
        }
    }

    static void mutex_1b() {

        Long shared = 5l;

        synchronized (shared) {
            System.out.println(++shared);
        }
    }

    static class Shared {
        Long value = 5l;

        public synchronized long inc() {
            //synchronized (this) {
                value++;
                return value;
            //}
        }
    }

    static void mutex_1c() {

        Shared shared = new Shared();
        shared.inc();
    }

    static void mutex_2() {

        AtomicLong shared = new AtomicLong(5);

        shared.incrementAndGet();
    }

    static void mutex_2bad() {

        AtomicLong x = new AtomicLong(5);
        AtomicLong y = new AtomicLong(5);

        x.incrementAndGet();
        y.incrementAndGet();
    }

    static void mutex_3() {

        long shared = 5l;
        Lock lockShared = new ReentrantLock();

        lockShared.lock();
        System.out.println(++shared);
        lockShared.unlock();
    }

    static void mutex_3ok() {

        long x = 4l,y = 5l;
        Lock lockShared = new ReentrantLock();

        lockShared.lock();
        System.out.println("X,Y: " + x++ + "," + y++);
        lockShared.unlock();
    }
}
