package cz.mendelu.ppr;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Kolekce {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        List<Long> data = Stream
                .iterate(1l, a -> a + 1)
                .limit(1000)
                .collect(Collectors.toList());
        List<Long> sData = Collections.synchronizedList(data);

        ExecutorService executor = Executors.newWorkStealingPool();
        List<Future<List<Long>>> futures = executor.invokeAll(Arrays.asList(
                () -> doubleItem(sData),
                () -> negativItem(sData)
        ));
        System.out.println(futures);
        System.out.println(futures.get(0).get());
        System.out.println(futures.get(1).get());
    }

    public static List<Long> doubleItem(List<Long> data) {
        List<Long> result = new LinkedList<>();
        synchronized (data) {
            while (!data.isEmpty()) {
                long i = data.remove(0);
                result.add(i * 2);
            }
        }
        return result;
    }

    public static List<Long> negativItem(List<Long> data) {
        List<Long> result = new LinkedList<>();
        while (!data.isEmpty()) {
            long i = data.remove(0);
            result.add(-i);
        }
        return result;
    }
}
