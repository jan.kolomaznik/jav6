package cz.mendelu.ppr;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParallelFor2 {

    public static void main(String[] args) throws InterruptedException {

        long start = System.currentTimeMillis();
        List<Long> data = Stream
                .iterate(1l, a -> a + 1)
                .limit(1000)
                .collect(Collectors.toList());
        System.out.println(System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
        List<Integer> sums = new ArrayList<>();
        for (Long number: data) {
            sums.add(digitSum(number));
        }
        System.out.println(System.currentTimeMillis() - start);
//        start = System.currentTimeMillis();
//        List<Integer> sum2 = data
//                .stream()
//                .map(ParallelFor2::digitSum)
//                .collect(Collectors.toList());
//        System.out.println(System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
        List<Integer> sum3 = data
                .parallelStream()
                .map(n -> digitSum(n))
                .collect(Collectors.toList());
        System.out.println(System.currentTimeMillis() - start);
        ExecutorService executor = Executors.newWorkStealingPool();
        start = System.currentTimeMillis();

        List<Callable<Integer>> calls = new ArrayList<>();
        for (Long number: data) {
            calls.add(() -> digitSum(number));
        }

        List<Future<Integer>> fetures = executor.invokeAll(calls);
//        fetures.forEach(f -> {
//            try {
//                f.get();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//        });
        System.out.println(System.currentTimeMillis() - start);
    }

    public static int digitSum(long number) {
        int sum = 0;
        while (number > 0) {
            sum += number % 10;
            number = number / 10;
        }
        long toTime = System.currentTimeMillis() + sum;
        while (System.currentTimeMillis() < toTime) {}
        return sum;
    }
}
