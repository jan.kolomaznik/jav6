package cz.mendelu.ppr;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class ProducentKonzument {

    public static BlockingDeque<Integer> blockingDeque = new LinkedBlockingDeque<>(10);

    public static void producent() {
        Thread current = Thread.currentThread();
        int i = 0;
        try {
            while (!current.isInterrupted()) {
                Worker.work(1000);
                if (blockingDeque.offer(i++, 5, TimeUnit.SECONDS)) {
                    System.out.printf("Vlozeni %d\n", i-1);
                } else {
                    System.out.printf("Plno %d\n", i-1);
                    current.interrupt();
                    break;
                }

                Thread.sleep(500);
            }
        } catch (InterruptedException e)  {
            e.printStackTrace();
        }
    }

    public static void consumer() {
        Thread current = Thread.currentThread();
        try {
            while (!current.isInterrupted()) {
                int i = blockingDeque.take();
                Worker.work(i * 500);
                Thread.sleep(500);
            }
        } catch (InterruptedException e)  {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Worker.sleep(10_000);
        new Thread(ProducentKonzument::producent, "Producent").start();
        new Thread(ProducentKonzument::consumer, "Konzument-0").start();
        //new Thread(ProducentKonzument::consumer, "Konzument-1").start();
        //new Thread(ProducentKonzument::consumer, "Konzument-2").start();
    }
}
