package cz.mendelu.ppr;

import javax.swing.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GUIVladkno {

    public static SwingWorker<String, Integer> swingWorker = new SwingWorker<String, Integer>() {
        @Override
        protected void process(List<Integer> chunks) {
            Thread current = Thread.currentThread();
            System.out.printf("%s: Proggres %s\n", current.getName(), chunks);
        }

        @Override
        protected void done() {
            Thread current = Thread.currentThread();
            System.out.printf("%s: Done\n", current.getName());
        }

        @Override
        protected String doInBackground() throws Exception {
            StringBuilder result = new StringBuilder();
            Thread current = Thread.currentThread();
            int i = 0;
            while (i < 10 && !current.isInterrupted()) {
                System.out.printf("%s: append %d\n", current.getName(), i);
                publish(i);
                result.append(i++).append("-");
                Worker.work(3000);
            }
            return result.toString();
        }
    };

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        JFrame frame = new JFrame();
        frame.setVisible(true);
        swingWorker.execute();
        System.out.printf("Result: %s\n", swingWorker.get());

    }
}
