package cz.mendelu.ppr;

public class Worker {

    /**
     * Aktivně pracovat po zadaný čas.
     * @param time jak dlouho.
     */
    public static void work(long time) {
        long finish = System.currentTimeMillis() + time;
        while (System.currentTimeMillis() < finish);
    }

    /**
     * Metoda pro uspání vlákna.
     * @param time na jak dlouho.
     */
    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            System.out.println("Sleep interrupted.");
        }
    }
}
