package cz.mendelu.ppr;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CtenariPisari {

    private static int data = 0;
    private static ReadWriteLock dataLock = new ReentrantReadWriteLock();

    public static void ctenar() {
        Thread current = Thread.currentThread();
        while (!current.isInterrupted()) {
            dataLock.readLock().lock();
            System.out.printf("%s: %d\n", current.getName(), data);
            Worker.work(2000);
            dataLock.readLock().unlock();
            Worker.sleep(1000);
        }
    }

    public static void pisar() {
        Thread current = Thread.currentThread();
        while (!current.isInterrupted()) {
            dataLock.writeLock().lock();
            System.out.printf("%s: %d\n", current.getName(), ++data);
            Worker.work(5000);
            dataLock.writeLock().unlock();
            Worker.sleep(10_000);
        }
    }

    public static void main(String[] args) {
        Worker.sleep(10_000);
        new Thread(CtenariPisari::pisar, "Pisar").start();
        for (int i = 0; i < 10; i++) {
            new Thread(CtenariPisari::ctenar, "Ctenar-" + i).start();
        }
    }

}
