package cz.mendelu.ppr;

public class KillThread {

    public static void main(String[] args) {
        Thread t = new Thread(KillThread::inThread);
        t.start();
        Worker.sleep(2500);
        t.interrupt();
    }

    public static void inThread() {
        int i = 0;
        Thread current = Thread.currentThread();
        try {
            while (!current.isInterrupted()) {
                System.out.printf("%s: %d\n", current.getName(), i++);
                Thread.sleep(500);
                if (i == 10) break;
            }
        } catch (InterruptedException e) {
            current.interrupt();
        }
        System.out.printf("%s interrupted: %s\n", current.getName(), current.isInterrupted());
        System.out.printf("%s: FINISHD\n", current.getName());
    }
}
