package cz.mendelu.ppr;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ParallelFor {

    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        List<Long> data = Stream
                .iterate(1l, a -> a + 1)
                .limit(10_000_000)
                .collect(Collectors.toList());
        System.out.println(System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
        List<Integer> sums = new ArrayList<>();
        for (Long number: data) {
            sums.add(digitSum(number));
        }
        System.out.println(System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
        List<Integer> sum2 = data
                .stream()
                .map(ParallelFor::digitSum)
                .collect(Collectors.toList());
        System.out.println(System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
        List<Integer> sum3 = data
                .parallelStream()
                .map(ParallelFor::digitSum)
                .collect(Collectors.toList());
        System.out.println(System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
    }

    public static int digitSum(long number) {
        int sum = 0;
        while (number > 0) {
            sum += number % 10;
            number = number / 10;
        }
        return sum;
    }
}
