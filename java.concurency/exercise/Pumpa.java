package jm.java.threads.exercise;

public class Pumpa extends Thread {

    private static Object trubka = new Object();

    private Nadrz a;
    private Nadrz b;

    public Pumpa(Nadrz a, Nadrz b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        while (!interrupted()) {
            synchronized (trubka) {
                a.dec();
                b.inc();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Nadrz n1 = new Nadrz();
        Nadrz n2 = new Nadrz();
        Pumpa p1 = new Pumpa(n1, n2);
        Pumpa p2 = new Pumpa(n2, n1);
        p1.start();
        p2.start();

        while (true) {
            Thread.sleep(1000);
            long a, b;
            synchronized (trubka) {
                a = n1.getObsah();
                b = n2.getObsah();
            }
            System.out.printf("Suma: %d, n1: %d, n2: %d\n", (a+b), a, b);
        }
    }
}
