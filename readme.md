JAVA CLEAN CODE: JAK PSÁT ČISTÝ KÓD V JAVĚ 
==========================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Tento kurz je určen zejména Java vývojářům, kteří již ovládají základy Javy a mají zkušenost s komerčním vývojem v týmu. 
Kurz vás naučí poznat špatně napsaný kód, vysvětlit, v čem je špatný. 
Naučíte se psát kód, který je čistý, efektivní a dlouhodobě udržovatelný. 
Vaše programátorské umění se tak posune na vyšší úroveň.

Osnova kurzu
------------
<div>

#### Náplň kurzu:

- **[Clean Code](/coding)**
    - Co je špatný kód a jaké má důsledky pro projekt
    - The Boy Scout Rule, Javadoc @author
- **[Smysluplné názvy](/coding.names)**
    - Názvy odhalující záměr, vyslovitelné a vyhledatelné názvy
    - Názvy tříd a metod, jak zabránit zmatení a nesrozumitelnosti
    - Využití pojmů z cílové oblasti, pravidlo jedno slovo na jeden koncept
    - Smysluplný a odůvodněný kontext
- **[Funkce](/coding.function)**
    - Malé funkce, jednoúčelové funkce, jedna úroveň abstrakce
    - Parametry funkcí, funkce s jedním, dvěma a více parametry
    - Vedlejší a nechtěné účinky funkcí
    - Výjimky vs chybové kódy, správné použití příkazu switch
    - Princip DRY, zamezení duplicity kódu
- **[Komentáře](/coding.comments)**
    - Princip dokumentace formou psaní kódu
    - Příklady dobrých komentářů
    - Příklady špatných komentářů
 - **Formátování kódu**
    - Smysl formátování a týmová pravidla
    - Vertikální formátování
    - Horizontální formátování
- **[Ošetřování chybových stavů](/coding.exeption)**
    - Využití výjimek, princip začít s try-catch-finally
    - Unchecked výjimky, přidání kontextu pro volající stranu
    - Princip nevracení a nepředávání hodnoty null
- **Hranice systému**
    - Použití kódu třetích stran, návrhový vzor adaptér
    - Objevování hranic systému, učební testy, jak mít jasné hranice
- **[Unit testy](/coding.testing)**
    - Tři zákony test driven development (TDD), BDD
    - Čisté testy, princip jeden test na jeden koncept, refaktorování
    - Využití frameworků JUnit, Mockito a AssertJ
- **[Třídy](/coding.types)**
    - Organizace třídy, psaní malých tříd, struktura připravená na změny
    - Princip jedné odpovědnosti (SRP - Single Responsibility Principle)
    - Objekty vs datové struktury, princip minimální znalosti (The Law of Demeter)
- **Systémy**
    - Oddělení konstrukce systému od jeho používání, oddělení zodpovědností
    - Škálování, AOP, optimalizace rozhodovací logiky
    - Návrh architektury systému řízený testy
- **Paralelní zpracování**
    - Kdy má v projektu smysl, výzvy spojené se souběžností
    - Defenzivní programování u souběžnosti
    - Důležitost znalosti zvolené technologie a použitých výpočetních modelů
    - Pravidlo pro malé kritické sekce, problém s ukončovacími rutinami
    - Testování více vláknového kódu

