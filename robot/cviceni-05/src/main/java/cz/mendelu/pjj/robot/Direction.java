package cz.mendelu.pjj.robot;

/**
 * Created by Honza on 09.11.2016.
 */
public enum Direction {

    NORTH(0, -1),
    NORTH_EAST(+1, -1),
    EAST(+1, 0),
    SOUTH(0, +1),
    WEST(-1, 0);

    private static final Direction[] VALUES = Direction.values();
    private static final int LENGTH = VALUES.length;

    private final int dx, dy;

    Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public Direction onLeft() {
        return VALUES[(ordinal() + 1) % LENGTH];
    }

    public Direction onRight() {
        return VALUES[(ordinal() - 1 + LENGTH) % LENGTH];
    }

    public Coordinate inDirectionFrom(Coordinate from) {
        return new Coordinate(from.x + dx, from.y + dy);
    }

}
