package cz.mendelu.pjj.robot;

import java.util.Objects;

/**
 * Created by Honza on 08.11.2016.
 */
public class Robot {

    private Coordinate coor;
    private Direction direction;

    public Robot(int x, int y, Direction direction) {
        this.coor = new Coordinate(x, y);
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof Robot)) return false;
        Robot robot = (Robot) o;
        return Objects.equals(coor, robot.coor) &&
                direction == robot.direction;
    }

    /**
     * Use this method do try destroy the enemy or something at target position.
     * @param coordinate
     */
    public void shootTo(Coordinate coordinate) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public boolean equalsBad(Object o) {
        boolean result = false;
        if (o != null) {
            if (this != o) {
                if (o instanceof Robot) {
                    Robot robot = (Robot) o;
                    result = Objects.equals(coor, robot.coor) &&
                            direction == robot.direction;
                }
            } else {
                result = true;
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coor, direction);
    }

    public void forward() {
        coor = direction.inDirectionFrom(coor);
    }

    public void turnLeft() {
        direction = direction.onLeft();
    }

    public void turnRight() {
        direction = direction.onRight();
    }

    public Coordinate getPosition() {
        return coor;
    }

    public String getDirectionName() {
        return direction.name();
    }

}
