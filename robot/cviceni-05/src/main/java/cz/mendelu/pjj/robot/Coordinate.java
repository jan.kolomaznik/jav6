package cz.mendelu.pjj.robot;

import java.util.Objects;

public class Coordinate {

    public final int x;
    public final int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }


}
