package jm.coding.name;

import java.time.LocalDate;

public class Person {


    private static class Name {

        public static Name of(String name) {
            return  null;
        }

        private String value;

        public Name(String value) {
            this.value = value;
        }
    }

    private LocalDate birthdate;
    private Name name;

    public Person(LocalDate birthdate, Name name) {
        this.birthdate = birthdate;
        this.name = name;
    }

    public static void main(String[] args) {
        Person p = new Person(LocalDate.now(), Name.of("Pepa"));
        p.name.split(" ")[0];
        p.name.getFirstName();
    }
}
